import { THEME_CHANGE } from './constants'

import { list } from './index'

const initialState = {
  list,
  selected: list[0]
}

export default (state = initialState, { type, value }) => {
  switch (type) {
    case THEME_CHANGE:
      return {
        ...state,
        selected: state.list.find(({ name }) => name === value)
      }
    default:
      return state
  }
}
