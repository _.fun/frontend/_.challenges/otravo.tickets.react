import React from 'react'

import { Box } from './containers'
import Themer from './themer'

const Profile = () => (
  <Box>
    <Themer />
  </Box>
)

export default Profile
