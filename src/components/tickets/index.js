import React from 'react'
import { connect } from 'react-redux'

import { Grid } from './containers'
import Filter, { FilterPropTypes } from './filter'
import List, { ListPropTypes } from './list'

import { filterAction, clearFilterAction } from './actions'

const Tickets = ({
  list,
  loading,
  filter,
  filterAction,
  clearFilterAction
}) => (
  <Grid>
    <Filter
      filter={filter}
      filterAction={filterAction}
      clearFilterAction={clearFilterAction}
    />
    <List loading={loading} list={list} />
  </Grid>
)

Tickets.propTypes = {
  ...FilterPropTypes,
  ...ListPropTypes
}

const mapStateToProps = ({ tickets: { list, loading, filter } }) => ({
  list,
  loading,
  filter
})
const mapDispatchToProps = dispatch => ({
  filterAction: filterAction(dispatch),
  clearFilterAction: clearFilterAction(dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Tickets)
