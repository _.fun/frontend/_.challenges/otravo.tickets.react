import styled from 'styled-components'

export const Box = styled.div`
  grid-area: filter;

  /* TODO: [feature] [theme] theme it */
  .react-datepicker__input-container > input {
    padding: 0.4rem;
  }
`
