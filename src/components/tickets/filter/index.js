import React from 'react'
import PropTypes from 'prop-types'
/* TODO: [feature] create your own datepicker */
/* TODO: [bug] it has a bugof dissapearing border in transition from mobile to desctop */
import DatePicker from 'react-datepicker'
import moment from 'moment'

import { Box } from './containers'

import 'react-datepicker/dist/react-datepicker.css'

const handleChange = action => value => action(value.format())

const Filter = ({ filter, filterAction, clearFilterAction }) => (
  <Box>
    <DatePicker
      selected={filter && moment(filter)}
      onChange={handleChange(filterAction)}
      minDate={moment()}
      isClearable={true}
      placeholderText="Selecte date"
      dateFormat="YYYY-MM-DD"
    />
  </Box>
)

export const FilterPropTypes = {
  filter: PropTypes.string, // optional
  filterAction: PropTypes.func.isRequired,
  clearFilterAction: PropTypes.func.isRequired
}

Filter.propTypes = FilterPropTypes

export default Filter
