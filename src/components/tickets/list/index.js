import React from 'react'
import PropTypes from 'prop-types'

import { Grid } from './containers'
import Loading from './loading'
import Empty from './empty'
import Genre, { GenrePropType } from './genre'

const List = ({ list, loading }) => {
  if (loading) return <Loading />
  if (list.length === 0) return <Empty />

  return (
    <Grid>
      {list.map(({ title, items }) => (
        <Genre key={title} title={title} items={items} />
      ))}
    </Grid>
  )
}

export const ListPropTypes = {
  loading: PropTypes.bool.isRequired,
  list: PropTypes.arrayOf(PropTypes.shape(GenrePropType)).isRequired
}

List.propTypes = ListPropTypes

export default List
