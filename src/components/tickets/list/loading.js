import React from 'react'

import LoadingGener from './genre/loading'

import { Grid } from './containers'

export default () => <Grid>{[0, 1].map(id => <LoadingGener key={id} />)}</Grid>
