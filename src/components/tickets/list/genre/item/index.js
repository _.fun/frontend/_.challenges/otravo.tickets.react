import React from 'react'
import PropTypes from 'prop-types'
import { FaClose } from 'react-icons/lib/fa'

import {
  Grid,
  Image,
  Header,
  Description,
  Date,
  Info,
  DummyImage
} from './containers'

const Item = ({
  name,
  dateStart,
  price,
  status,
  ticketsLeft,
  ticketsAvailable
}) => (
  <Grid>
    <Image>
      <DummyImage>
        <FaClose />
      </DummyImage>
    </Image>
    <Header>
      <h3>{name}</h3>
    </Header>
    <Description>
      <span>Tickets left: {ticketsLeft}</span>
      <br />
      <span>Tickets available: {ticketsAvailable}</span>
      <br />
      <span>Status: {status}</span>
    </Description>
    <Date>starts on {dateStart.format('YYYY-MM-DD')}</Date>
    <Info>Price {price}$</Info>
  </Grid>
)

export const ItemPropType = {
  name: PropTypes.string.isRequired,
  dateStart: PropTypes.shape({}).isRequired, // leave for now as it's a moment object
  price: PropTypes.number.isRequired,
  status: PropTypes.string.isRequired,
  ticketsLeft: PropTypes.number.isRequired,
  ticketsAvailable: PropTypes.number.isRequired
}

Item.propTypes = ItemPropType

export default Item
