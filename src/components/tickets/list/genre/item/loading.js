import React from 'react'

import { Block } from 'components/common/loading'
import { Grid, Image, Header, Description, Date, Info } from './containers'

export default () => (
  <Grid>
    <Image />
    <Header>
      <Block isHalfSize />
    </Header>
    <Description>
      <Block />
    </Description>
    <Date>
      <Block isSmall />
    </Date>
    <Info>
      <Block isSmall />
    </Info>
  </Grid>
)
