import React from 'react'
import PropTypes from 'prop-types'

import { Grid, Header, Content } from './containers'
import Item, { ItemPropType } from './item'

const Genre = ({ title, items }) => (
  <Grid>
    <Header>
      <h2>{title}</h2>
    </Header>
    <Content>{items.map(item => <Item key={item.name} {...item} />)}</Content>
  </Grid>
)

export const GenrePropType = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.shape(ItemPropType)).isRequired
}

Genre.propTypes = GenrePropType

export default Genre
