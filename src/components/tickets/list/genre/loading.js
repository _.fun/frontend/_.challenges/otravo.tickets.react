import React from 'react'
import { Block } from 'components/common/loading'
import LoadingItem from './item/loading'

import { Grid, Header, Content } from './containers'

export default () => (
  <Grid>
    <Header>
      <Block isMedium isHalfSize />
    </Header>
    <Content>{[0].map(id => <LoadingItem key={id} />)}</Content>
  </Grid>
)
