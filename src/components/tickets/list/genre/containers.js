import styled from 'styled-components'

export const Grid = styled.div`
  display: grid;

  grid-gap: 1rem;
  grid-template: 'header' 'content';
`

export const Header = styled.div`
  grid-area: header;
`

export const Content = styled.div`
  grid-area: content;

  display: grid;
  grid-gap: 1rem;
  grid-auto-flow: row;
`
