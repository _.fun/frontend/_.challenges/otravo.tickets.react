import * as types from './constants'

export const initialState = {
  list: [],
  loading: false,
  filter: undefined
}

export default (state = initialState, { type, value }) => {
  switch (type) {
    case types.TICKETS_LIST_UPDATE:
      return {
        ...state,
        list: value
      }
    case types.TICKETS_FILTER_CHANGE:
      return {
        ...state,
        filter: value
      }
    case types.TICKETS_LIST_LOADING:
      return {
        ...state,
        loading: value
      }
    default:
      return state
  }
}
