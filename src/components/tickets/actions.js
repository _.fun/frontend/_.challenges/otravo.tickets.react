import * as types from './constants'

export const filter = value => ({
  type: types.TICKETS_FILTER_CHANGE,
  value
})

export const listLoading = value => ({
  type: types.TICKETS_LIST_LOADING,
  value
})

export const listUpdate = value => ({
  type: types.TICKETS_LIST_UPDATE,
  value
})

export const filterAction = dispatch => value => dispatch(filter(value))

export const clearFilterAction = dispatch => () => dispatch(filter(undefined))
