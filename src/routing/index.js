import Tickets from 'components/tickets'
import Profile from 'components/profile'

export const configuration = {
  redirect: '/tickets'
}

export const routes = [
  {
    name: 'tickets',
    title: 'tickets',
    path: '/tickets',
    component: Tickets
  },
  {
    name: 'profile',
    title: 'profile',
    path: '/profile',
    component: Profile
  }
]
