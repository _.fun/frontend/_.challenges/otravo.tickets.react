import { combineEpics } from 'redux-observable'

import phonebook from 'components/tickets/epic'

export default combineEpics(phonebook)
