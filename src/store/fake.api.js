// NOTE: normally this thing doesn't exist, so don't pay attention to it
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of'
import moment from 'moment'

const data = [
  {
    name: 'Something',
    dateStart: moment().add(20, 'days'),
    gener: 'Comedy',
    status: 'open for sale',
    ticketsLeft: 100,
    ticketsAvailable: 5,
    price: 50
  },
  {
    name: 'Something 2',
    dateStart: moment().add(25, 'days'),
    gener: 'Action',
    status: 'preorder',
    ticketsLeft: 100,
    ticketsAvailable: 5,
    price: 40
  }
]

const groupBy = (list, keyGetter) => {
  const map = new Map()

  list.forEach(item => {
    const key = keyGetter(item)
    const collection = map.get(key)

    if (!collection) map.set(key, [item])
    else collection.push(item)
  })

  return Array.from(map)
}

const filteredData = date =>
  Observable.of(
    groupBy(
      data.filter(
        ({ dateStart }) => Math.abs(dateStart.diff(date, 'days')) <= 90
      ),
      item => item.gener
    ).map(([title, items]) => ({ title, items }))
  )

export const fakeAjaxGet = url => {
  if (url.startsWith('/api/tickets/')) {
    const parameter = url.split('/').slice(-1)[0]
    const date = moment(parameter) // by last element
    if (date.isValid()) return filteredData(date)
  }

  return Observable.of(
    groupBy(data, item => item.gener).map(([title, items]) => ({
      title,
      items
    }))
  )
}
