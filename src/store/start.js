import { clearFilterAction } from 'components/tickets/actions'

export default ({ dispatch }) => {
  clearFilterAction(dispatch)()
}
