# Otravo challenge

## Setup

### General

This application was created using `create-react-app`. Instructions to run this application is the same as in all `create-react-app` applications.

Before running local development check that your `npm` is up to date. Use [documentation](https://www.npmjs.com/get-npm) for installation and upgrade guide.

### Local

For local run you should execute commands:

```shell
npm i
npm start
```

### Local production

To serve application locally you should exacute commands using `serve`:

```shell
npm i -g serve
npm run build --production
serve -s build
```

### Docker

For fun I've added `docker` with `docker-compose` with development container build. It's unoptimized. Make sure that `docker` is install on your machine, before running, use [installation guide](https://docs.docker.com/install/).

```shell
docker-compose build
docker-compose up
```

## Description

### Task

Need to create shows `tickets` sale application.

UI consists of `filter` and `list` of `tickets`, grouped by `genre`.

On filter change we should load tickets data.

`Ticket` should show data:

- Title of the show
- Tickets left for the performance of the show on the show date
- Tickets available for sale
- Status of the ticket sale
- NEW: Price of a ticket

### Help

- `fake.api.js` contains api data, in case you would want to add more `tickets`.

### Notes

- on first load `tickets` are loaded using `filterChange` action with empty filter, which produces a warning, which should be ignored because of `fake.api` and `moment`
- For some reason `epics` `unit tests` are failing with timout, couldn't find information regarding it in sensible timeframe.
- `tickets/*` components are cleaned for the review, other components are made for fast `prototyping` and still `under development` in template